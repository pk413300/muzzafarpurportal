const Footer = () => {
return(
    <>
    <footer className="site-footer" >
        <div className="container">
          <div className="row">
            <div className="col-md-3 col-sm-6 col-xs-12 fbox">
              <h4>About Us</h4>
              <ul className>
                <li>
                  <a href="#" title>
                    Project Details{" "}
                  </a>
                </li>
                <li>
                  <a href="#" title>
                    Project Status{" "}
                  </a>
                </li>
                <li>
                  <a href="#" title>
                    Ongoing Project
                  </a>
                </li>
                <li>
                  <a href="#" title>
                    Commissioner
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-md-3 col-sm-6 col-xs-12 fbox">
              <h4>General Information</h4>
              <ul className>
                <li>
                  <a href="#" title>
                    ULB Profile
                  </a>
                </li>
                <li>
                  <a href="#" title>
                    Organization Chart
                  </a>
                </li>
                <li>
                  <a href="#" title>
                    Ward Profile
                  </a>
                </li>
                <li>
                  <a href="#" title>
                    Budget &amp; Finance
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-md-3 col-sm-6 col-xs-12 fbox">
              <h4>General Information</h4>
              <ul className>
                <li>
                  <a href="#" title>
                    Act/Bye Laws
                  </a>
                </li>
                <li>
                  <a href="#" title>
                    Important Links
                  </a>
                </li>
                <li>
                  <a href="#" title>
                    Development
                  </a>
                </li>
                <li>
                  <a href="#" title>
                    Welfare
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-md-3 col-sm-6 col-xs-12 fbox">
              <h4 />
              <img src="img/fo1.png" style={{ width: "150px" }} />
            </div>
          </div>
        </div>
        <div id="copyright">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <p className="pull-left">
                  © Copyright @ 2019. MMC,
                  <br /> All Rights Reserved
                </p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    
    </>
)
}

export default Footer;