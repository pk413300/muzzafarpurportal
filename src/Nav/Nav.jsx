import { useState } from "react";
import { Link } from "react-router-dom";
import getAcessTokenApi from "../Api/getAcessToken.api";
import getDataLogger from "../Api/getDataLogger";
import "./Nav.css";
import { useDispatch } from "react-redux";
import { handleAeronData } from "../actions/actions";
import Modal from "../Modal/Modal"


const Nav = () => {
  const dispatch = useDispatch();
  const [className, setClassName] = useState("0");
  const [loading, setLoading] = useState(false);


  const accessToken = async (usn, usnName) => {

    const val = await getAcessTokenApi.getAcessToken();

    logData(val, usn, usnName);
  };

  const logData = async (val, usn, usnName) => {
    let daata = await getDataLogger.getData(val, usn);
    daata = {
      daata,
      "name": usnName
    }

    if (daata) {

      setLoading(false)
      dispatch(handleAeronData(daata));
    }




  }


  return (
    <>
      {
        loading ? <Modal />  : ""
      }
      <header className="site-header" style={{ zIndex: "1" }}>
        <div className="top">
          <div className="container">
            <div className="row">
              <div className="col-sm-6 col-md-6">
                <i className="fa fa-sun-o" aria-hidden="true"></i> &nbsp;&nbsp;{" "}
                <i className="fa fa-adjust" aria-hidden="true"></i> &nbsp;&nbsp;
                A- &nbsp;&nbsp; A &nbsp;&nbsp; A+
              </div>
              <div className="col-sm-6  col-md-6">
                <ul className="list-inline pull-right">
                  <li>
                    <a
                      href="#"
                      style={{ backgroundColor: "#f47f20", padding: "3px" }}
                    >
                      LOGIN
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <nav className="navbar navbar-default">
          <div
            className="container"
            style={{ backgroundImage: "url(img/b.png)", height: "80px" }}
          >
            <button
              type="button"
              className="navbar-toggle collapsed"
              data-toggle="collapse"
              data-target="#bs-navbar-collapse"
            >
              <span className="sr-only">Toggle Navigation</span>
              <i className="fa fa-bars"></i>
            </button>
            <a href="index.html" className="navbar-brand">
              <img
                src="img/logo-01-1-1.png"
                style={{ width: "450px" }}
                alt="Post"
              ></img>
            </a>
          </div>
        </nav>

        <nav
          className="navbar navbar-default"
          style={{ backgroundColor: "#fff" }}
        >
          <div className="container">
            <div className="collapse navbar-collapse" id="bs-navbar-collapse">
              <ul className="nav navbar-nav main-navbar-nav">
                <li
                  className={className === "0" ? "active" : ""}
                  onClick={() => {
                    setClassName("0");
                  }}
                >
                  <Link to="/">
                    {/* <a href="#" title=""> */}
                    Home
                    {/* </a> */}
                  </Link>
                </li>

                <li
                  className={
                    className === "1"
                      ? "active nav-item dropdown"
                      : "nav-item dropdown"
                  }
                >
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    About Us <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    style={{ backgroundColor: "#2eae9f" }}
                    aria-labelledby="navbarDropdown"
                  >
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("1");
                      }}
                    >
                      <Link to="/aboutcity">
                        <div>About City</div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("1");
                      }}
                    >
                      <Link to="/cityprofile">
                        <div>City Profile</div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("1");
                      }}
                    >
                      <Link to="/team">
                        <div>Team of MSCL</div>
                      </Link>
                    </li>
                  </ul>
                </li>

                <li>
                  <a href="#" title="">
                    Departments <b className="caret"></b>
                  </a>
                </li>

                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    e-Services <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    style={{ backgroundcColor: "#2eae9f" }}
                    aria-labelledby="navbarDropdown"
                  >
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("3");
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>Grievance </div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("3");
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>Property Tax </div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("3");
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>Building Permission</div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("3");
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>Trade and License</div>
                      </Link>
                    </li>
                  </ul>
                </li>

                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    e-Payments <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    style={{ backgroundColor: "#2eae9f " }}
                    aria-labelledby="navbarDropdown"
                  >
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("3");
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>Pay Property tax </div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("3");
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>Trade and License </div>
                      </Link>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="#" title="">
                    Dashboard <b className="caret"></b>
                  </a>
                </li>
                <li
                  className={className === "2" ? "active" : ""}
                  onClick={() => {
                    setClassName("2");
                  }}
                >
                  <Link to="/contact">
                    <div>Contact Us </div>
                  </Link>
                </li>
                <li>
                  <a href="#" title="">
                    Careers <b className="caret"></b>
                  </a>
                </li>

                <li
                  className="nav-item dropdown"
                  onClick={() => {
                    setClassName("3");
                  }}
                >
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Information <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    style={{ backgroundColor: "#2eae9f" }}
                    aria-labelledby="navbarDropdown"
                  >
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("3");
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>Event </div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("3");
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>Geo & Circular </div>
                      </Link>
                    </li>
                  </ul>
                </li>

                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Tenders <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    style={{ backgroundColor: "#2eae9f" }}
                    aria-labelledby="navbarDropdown"
                  >
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("3");
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>Open Tender </div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName("3");
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>Closed Tender </div>
                      </Link>
                    </li>
                  </ul>
                </li>
                {/* <li
                  className={className === "2" ? "active" : ""}
                  onClick={() => {
                    setClassName("2");
                  }}
                >
                  <a


                  >
                    <div
                     
                    >
                      Environment
                    </div>
                  </a>
                </li> */}
                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"

                  >
                    Environment <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    style={{ backgroundColor: "#2eae9f" }}
                    aria-labelledby="navbarDropdown"
                  >
                    <li
                      className="drop"
                      // onClick={() => {
                      //   setClassName("3");
                      // }}
                      onClick={() => {

                        accessToken("240091642999458", "MARWARI SCHOOL");
                        setLoading(true)


                      }}
                    >
                      <Link to="/environment">
                        <div>MARWARI SCHOOL </div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      // onClick={() => {
                      //   setClassName("3");
                      // }}
                      onClick={() => {

                        accessToken("240091643271140", "MIT LAXMI CHOWK");
                        setLoading(true)

                      }}
                    >
                      <Link to="/environment">
                        <div>MIT LAXMI CHOWK </div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      // onClick={() => {
                      //   setClassName("3");
                      // }}
                      onClick={() => {

                        accessToken("240091644495292", "LN MISHRA COLLEGE");
                        setLoading(true)

                      }}
                    >
                      <Link to="/environment">
                        <div>LN MISHRA COLLEGE </div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      // onClick={() => {
                      //   setClassName("3");
                      // }}
                      onClick={() => {

                        accessToken("240091642485088", "RBTS");
                        setLoading(true)

                      }}
                    >
                      <Link to="/environment">
                        <div>RBTS </div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      // onClick={() => {
                      //   setClassName("3");
                      // }}
                      onClick={() => {

                        accessToken("240091639638721", "BSPCB");
                        setLoading(true)

                      }}
                    >
                      <Link to="/environment">
                        <div>BSPCB </div>
                      </Link>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    </>
  );
};

export default Nav;
