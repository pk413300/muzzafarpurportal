import "./modal.css";
const Modal = () => {
    return (
        <>
            <div
                id="loading_container"

            >
                <div className="container-fluid">
                    <div id="overlay"></div>
                    <div className="row">
                        <div
                            className="col-md-12"
                            id="spinner"

                        >
                            <div className="row">
                                <div className="col-md-4"></div>
                                <div className="col-md-4">
                                    <h1 className="text-center space loading_Text">Loading...</h1>
                                </div>
                                <div className="col-md-4"></div>
                            </div>
                            <div className="row">
                                <div className="col-md-4"></div>
                                <div className="col-md-4">
                                    <div className="spinner2">
                                        <div className="rect1" />
                                        <div className="rect2" />
                                        <div className="rect3" />

                                    </div>
                                </div>
                                <div className="col-md-4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Modal;
