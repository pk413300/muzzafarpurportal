import React, { useState, useEffect } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useSelector } from "react-redux";
import Modal from "../Modal/Modal"

const Environment = () => {
    const [loading, setLoading] = useState(true)
   
    const reduxStore = useSelector((state) => {

        return state?.reducerData?.Dataloger;
    });
    useEffect(() => {
        if(reduxStore){
            setLoading(false)
        }
    },[reduxStore])




    let dataShower = reduxStore?.daata?.data?.params
    let time = reduxStore?.daata?.data?.timestamp?.time
    let date = reduxStore?.daata?.data?.timestamp?.date
    let Name = reduxStore?.name;


    return (
        <>{
             loading ? <Modal/> : <section className="main-body" style={{ padding: "30px 0px", display: "flex", justifyContent: "center", alignItems: "center" }}>
             <div style={{
                 width: "70%", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center"
             }}>
                 <div style={{
                     display: "flex", justifyContent: "center", alignItems: "center"
                 }}>

                     <div style={{ margin: "30px", fontSize: "30px", color: "black" }}>Center : {Name}</div>
                     <div style={{ margin: "30px", fontSize: "30px", color: "black" }}>Time : {time}</div>

                     <div style={{ margin: "30px", fontSize: "30px", color: "black" }}>Date : {date}</div>
                 </div>
                 <TableContainer component={Paper} >
                     <Table sx={{ minWidth: 650 }} aria-label="simple table">
                         <TableHead>
                             <TableRow style={{ fontSize: "20px", backgroundColor: "#2eae9f" }}>
                                 <TableCell style={{ fontSize: "20px", color: "#ffff" }}>Caption</TableCell>
                                 <TableCell align="right" style={{ fontSize: "20px", color: "#ffff" }}>Unit</TableCell>
                                 <TableCell align="right" style={{ fontSize: "20px", color: "#ffff" }}>Value</TableCell>

                             </TableRow>
                         </TableHead>
                         <TableBody>
                             {dataShower?.map((row) => (
                                 <TableRow
                                     key={row?.name}
                                     sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                 >
                                     <TableCell component="th" scope="row" style={{ fontSize: "15px" }} >
                                         {row?.caption}
                                     </TableCell>

                                     <TableCell align="right" style={{ fontSize: "15px" }} >{row?.unit}</TableCell>
                                     <TableCell align="right" style={{ fontSize: "15px" }}>{row?.value}</TableCell>

                                 </TableRow>
                             ))}
                         </TableBody>
                     </Table>
                 </TableContainer>
             </div>
         </section>

        }

        </>

    )
}

export default Environment;