import "./MSCL.css"


const MSCL = () => {
  return (
      <>
     
      <section className="main-body" style={{ padding: "30px 0px" }}>
        <div className="container headings-about">
          <h4 className="content-title-2" style={{ fontWeight: 670, color: "#3f3f3f !important" }}>
            TEAM OF MSCL
          </h4>
          <div>
            <div className="container-fluid">
              <div className="row">
                <div className="col-sm-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        Vivek Ranjan Maitrey, IAS
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        Managing Director
                      </h6>
                      <p className="card-text" style={{ color: "#3f3f3f" }}></p>
                      <p style={{ color: "#3f3f3f" }} />
                      <p />
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        SRI BHUDEB CHAKRAVARTI
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#532323" }}
                      >
                        CEO
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        mscl.ceo@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        RAJESH SINHA
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        CGM
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        mscl.cgm@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        MD. NAUSHAD
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        CFO
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        cfo.mscl@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        SRI AJAY KUMAR
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        SR. MANAGER (Tech)
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        ajaykrpatna74@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        SRI PREM DEO SHARMA
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        SR. MANAGER (Tech)
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        prem_deo@rediffmail.com
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        AJAY KUMAR
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        (M&amp;E)
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        manager.monitoring1mscl@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        {" "}
                        SHASHANK JHA{" "}
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        MANAGER (Tech)
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        shashank78mscl@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        CHANDNI
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        MANAGER (Tech)
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        chandni09118@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        SUBHASH KUMAR PANDIT
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        MANAGER (Tech)
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        subhashchandra1190@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        RAJEEV RANJAN
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        COMPANY SECRETARY
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        rajivranjancs@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        SRI ABHISHEK RAMAN
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        MANAGER (IT)
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        itmanager.mscl@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row row_container_last">
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        MD. ROOHNAWAZ HANZALA
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        MANAGER (IMPL. &amp; CONTROL)
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        mscl.manager.ic@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="card"
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png"
                      className="card-img-top image_container"
                      alt="..."
                    />
                    <div className="card-body card_text_align">
                      <h5
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        SRI PRADEEP KUMAR SAH
                      </h5>
                      <h6
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        MANAGER (FINANCE &amp; PROCUREMENT)
                      </h6>
                      <p
                        className="card-text"
                        style={{ fontWeight: 600, color: "#3f3f3f" }}
                      >
                        managerfnp.mscl@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div></div>
            <div></div>
          </div>
        </div>
      </section>
   
    </>
  );
};
export default MSCL;
