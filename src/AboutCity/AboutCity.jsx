const AboutCity = () => {
  return (
    <>
      <section className="main-body" style={{ padding: "30px 0px" }}>
        <div className="container headings-about">
          <h4 className="content-title-2" style={{ fontWeight: 670, color : "#3f3f3f3" }} >
            ABOUT CITY
          </h4>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            Muzaffarpur is the fourth most populous city in Bihar. It is famous
            for Shahi lychees and is known as the Lychee Kingdom. ‘The Land Of
            Leechi’ was created in 1875 for the sake of administrative
            convenience by splitting up the earlier district of Tirhut. There
            the most famous temple in Muzaffarpur is Baba Garib Sthan Mandir. It
            is known as the Deoghar of Bihar. As of the 2011 India census,
            Muzaffarpur have a population of 393,724.
          </p>
        </div>
        <div style={{height : "58px"}}></div>
      </section>
    </>
  );
};

export default AboutCity;
