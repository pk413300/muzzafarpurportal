import "./ContactUs.css"

const ContactUS = () => {
  return (
    <>
    
      <section className="main-body" style={{ padding: "30px 0px" }}>
        <div className="container headings-about">
          <h4 className="content-title-2" style={{ fontWeight: 670 }}>
            CONTACT US
          </h4>
          <div className="row">
            <div className="col-md-4">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1719.2827432497768!2d85.37853456083796!3d26.125934157047404!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ed11c0b048f7cf%3A0x88360e0a94595ebc!2sMuzaffarpur%20Smart%20City%20Limited!5e0!3m2!1sen!2sin!4v1651147350440!5m2!1sen!2sin  "
                style={{ border: 0, width: "100%", height: "auto", height : "300px" }}
                allowFullScreen
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
              />
            </div>
            <div className="col-md-3">
              <div className="row-map">
                <i className="bi bi-geo-alt" />
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={25}
                  height={25}
                  fill="currentColor"
                  className="bi bi-geo-alt map_icon"
                  viewBox="0 0 16 16"
                >
                  <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z" />
                  <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                </svg>

                <h4>
                  MSCL, Room No 10, 24, Combined Building, First Floor, Bihar
                  842001.
                </h4>
              </div>
            </div>
          </div>
        </div>
      </section>
     
    </>
  );
};

export default ContactUS;
