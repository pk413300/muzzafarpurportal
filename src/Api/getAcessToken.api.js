const axios = require("axios");

export default  {
  getAcessToken: async () => {
    var config = {
      method: "post",
      url: "https://getiotservice.aeronsystems.com:9007/v1.0/gettoken",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Accept: "application/json",
        Authorization: "Basic YWVyb25hcGk6Ym05eVpXRXlNREV5",
      },
      data: "username=anit1973&password=Muz_admin@2022&grant_type=password",
    };

    try {
      let res = await axios(config);

      return res?.data?.access_token;
    } catch (error) {
      throw error;
    }
  },
};
