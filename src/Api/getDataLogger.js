const axios = require("axios");

export default {
  getData: async (token, usn) => {
    var config = {
      method: "get",
      url: `https://getiotservice.aeronsystems.com:9007/v1.0/getdata?usn=${usn}`,
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
    };

    try {
      let res = await axios(config);

      return res;
    } catch (error) {
      console.log("error thrown");
      throw error;
    }
  },
};
