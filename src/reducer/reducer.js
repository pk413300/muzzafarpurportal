import { combineReducers } from "redux";

import reducerData from "./reducerData";

export const reducers = combineReducers({
  reducerData,
});
