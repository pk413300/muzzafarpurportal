const initialState = {};

const reducerData = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_AERON_DATA":
      const Dataloger = action.payload;

      return {
        ...state,
        Dataloger,
      };

    default:
      return state;
  }
};
export default reducerData;
