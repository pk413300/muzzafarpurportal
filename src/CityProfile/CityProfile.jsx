const CityProfile = () => {
  return (
    <>
      <section className="main-body" style={{ padding: "30px 0px" }}>
        <div className="container headings-about">
          <h4 className="content-title-2" style={{ fontWeight: 670 }}>
            CITY PROFILE
          </h4>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            The Muzaffarpur Smart City Limited (MSCL) is the Special Purpose
            Vehicle (SPV) constituted as per directives of Ministry of Urban
            Development (MoUD), Government of India (GoI) for executing SMART
            CITY MISSION (SCM) in Muzaffarpur. MSCL has been established under
            the Companies Act, 2013 of the Ministry of Corporate Affairs,
            Government of India. It is supported by PMC, PMU and the
            implementing agency for the implementation of the mission. The Smart
            City planning was done with the strength-full and firm belief for
            the cultural and spiritual importance of the city along-with PAN
            city technological intervention to rejuvenate and re-live in a
            smarter way through various smart solutions for Muzaffarpur.
          </p>
        </div>
        <div style={{height : "37px"}}></div>
      </section>
    </>
  );
};

export default CityProfile;
