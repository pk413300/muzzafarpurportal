import UnderDev from "./UnderDev/UnderDev";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import React, { Suspense } from "react";
// import Home from "./Home/Home";
// import Nav from "./Nav/Nav";
// import Footer from "./Footer/Footer";
// import AboutCity from "./AboutCity/AboutCity";
// import CityProfile from "./CityProfile/CityProfile";
// import Environment from "./Environment/Environment";
// import MSCL from "./MSCL/MSCL";
// import ContactUS from "./ContactUS/ContactUS";
const Home = React.lazy(() => import("./Home/Home"));
const Nav = React.lazy(() => import("./Nav/Nav"));
const Footer = React.lazy(() => import("./Footer/Footer"));
const AboutCity = React.lazy(() => import("./AboutCity/AboutCity"));
const CityProfile = React.lazy(() => import("./CityProfile/CityProfile"));
const Environment = React.lazy(() => import("./Environment/Environment"));
const MSCL = React.lazy(() => import("./MSCL/MSCL"));
const ContactUS = React.lazy(() => import("./ContactUS/ContactUS"));



const App = () => {
  return (
    <>
      <Suspense fallback={<div>LOADING...</div>}>
        <Router>
          <Nav />

          <Routes>
            <Route path="/environment" element={<Environment />}></Route>
            <Route path="/aboutcity" element={<AboutCity />}></Route>
            <Route path="/cityprofile" element={<CityProfile />}></Route>
            <Route path="/team" element={<MSCL />}></Route>
            <Route path="/contact" element={<ContactUS />}></Route>
            <Route path="/underconstruction" element={<UnderDev />}></Route>
            <Route path="/" element={<Home />}></Route>
          </Routes>
          <Footer />
        </Router>
      </Suspense>
    </>
  );
};

export default App;
